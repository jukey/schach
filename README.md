## lichess.org Anleitungen

### Eigene Partien herunterladen

1. Meldet euch auf der Seite an oder loggt euch ein.
2. Klickt ganz oben rechts auf euren Benutzernamen ([im Bild: 1.]((https://gitlab.com/jukey/schach/blob/master/bilder/lichess_01.png)
)) und dann auf Profil ([im Bild: 2.]((https://gitlab.com/jukey/schach/blob/master/bilder/lichess_01.png)
))
3. Ihr könnt die Partien nun durch klick auf das entsprechende
Symbolherunterladen ([im Bild: 3.](https://gitlab.com/jukey/schach/blob/master/bilder/lichess_02.png))
4. Als letzten Zwischenschritt müsst ihr nun die einfache Schach-Aufgabe
lösen ([im Bild: 4.](https://gitlab.com/jukey/schach/blob/master/bilder/lichess_03.png)) und auf **Partien exportieren** ([im Bild: 5.](https://gitlab.com/jukey/schach/blob/master/bilder/lichess_03.png)) klicken.

Nun könnt Ihr die Partien nach Herzenslust mit einem Programm eurer Wahl
analysieren.
